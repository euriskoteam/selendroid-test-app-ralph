package com.mobile;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.util.ExcelReader;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class SelendroidTest {

	AppiumDriver<WebElement> driver;
	List dataFromExcel;
	String[] splitData;

	public int getUserLanguage(String progLanguage) {
		switch (progLanguage) {
		case "Ruby":
			return 1;
		case "Php":
			return 2;
		case "Scala":
			return 3;
		case "Python":
			return 4;
		case "Javascript":
			return 5;
		case "Java":
			return 6;
		case "C++":
			return 7;
		case "C#":
			return 8;
		default:
			return 1;
		}
	}

	@BeforeClass
	public void Setup() throws IOException {

		// Set capabilities and initialize driver

		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus_5X_API_28");
		cap.setCapability(MobileCapabilityType.APP, "C:/Users/Ralph/Downloads/selendroid-test-app-0.17.0.apk");
		cap.setCapability(MobileCapabilityType.UDID, "emulator-5554");
		cap.setCapability("avd", "Nexus_5X_API_28");

		driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);

		dataFromExcel = ExcelReader.ReadExcelFile();
		splitData = dataFromExcel.get(0).toString().split(",");
	}

	@BeforeMethod
	public void BeforeMethod() {

		// Check if driver is initialized before each method

		Assert.assertNotNull(driver.getContext());
	}

	@Test
	public void CloseVersionWarning() throws InterruptedException {

		// It closes the Android version warning dialog box

		WebElement element = driver.findElement(By.id("android:id/button1"));
		element.click();

	}

	@Test
	public void DisplayTextView() throws InterruptedException {

		// It clicks on the 'Display text view' button
		// and checks if the text is displayed

		TimeUnit.SECONDS.sleep(3);
		WebElement element = driver.findElement(By.id("io.selendroid.testapp:id/visibleButtonTest"));
		element.click();

		TimeUnit.SECONDS.sleep(2);
		element = driver.findElement(By.id("io.selendroid.testapp:id/visibleTextView"));
		element.equals("Text is sometimes displayed");

	}

	@Test
	public void DisplayToast() throws InterruptedException {

		// It clicks on the 'Display a toast' button

		TimeUnit.SECONDS.sleep(3);
		WebElement element = driver.findElement(By.id("io.selendroid.testapp:id/showToastButton"));
		element.click();

	}

	@Test
	public void RegisterUser() throws InterruptedException {

		// It clicks on the 'Show Progress Bar for a while' button
		// Get data from excel sheet
		// Input data accordingly

		TimeUnit.SECONDS.sleep(3);
		WebElement element = driver.findElement(By.id("io.selendroid.testapp:id/waitingButtonTest"));
		element.click();

		TimeUnit.SECONDS.sleep(15);

		driver.navigate().back();
		element = driver.findElement(By.id("io.selendroid.testapp:id/inputUsername"));
		element.sendKeys(splitData[0]);

		element = driver.findElement(By.id("io.selendroid.testapp:id/inputEmail"));
		element.sendKeys(splitData[1]);

		element = driver.findElement(By.id("io.selendroid.testapp:id/inputPassword"));
		element.sendKeys(splitData[2]);

		element = driver.findElement(By.id("io.selendroid.testapp:id/inputName"));
		element.clear();
		element.sendKeys(splitData[3]);

		element = driver.findElement(By.id("io.selendroid.testapp:id/input_preferedProgrammingLanguage"));
		element.click();
		TimeUnit.SECONDS.sleep(2);

		element = driver.findElement(By.xpath(
				"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ListView/android.widget.CheckedTextView["
						+ getUserLanguage(splitData[4]) + "]"));
		element.click();

		TimeUnit.SECONDS.sleep(2);
		element = driver.findElement(By.id("io.selendroid.testapp:id/btnRegisterUser"));
		element.click();

		TimeUnit.SECONDS.sleep(2);
		element = driver.findElement(By.id("io.selendroid.testapp:id/buttonRegisterUser"));
		element.click();

	}

	@Test
	public void TestTextField() throws InterruptedException {

		// It inputs data into the text field

		TimeUnit.SECONDS.sleep(3);
		WebElement element = driver.findElement(By.id("io.selendroid.testapp:id/my_text_field"));
		element.sendKeys("Thank you for your tests");

	}

	@Test
	public void TouchTest() throws InterruptedException {

		// It checks if a single tap was made

		TimeUnit.SECONDS.sleep(3);
		WebElement element = driver.findElement(By.id("io.selendroid.testapp:id/touchTest"));

		TimeUnit.SECONDS.sleep(2);
		element = driver.findElement(By.id("io.selendroid.testapp:id/LinearLayout1"));
		element.click();

		TimeUnit.SECONDS.sleep(3);
		element = driver.findElement(By.id("	io.selendroid.testapp:id/gesture_type_text_view"));
		element.equals("SINGLE TAP CONFIRMED");

	}

	@AfterClass
	public void CloseApp() {
		driver.quit();
	}
}

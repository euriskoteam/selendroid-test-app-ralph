# Selendriod Test App QA

Selendroid Test App QA applies several test cases on the Selendriod Test app. The below guidelines will help you setup the test environment.

## Getting Started

First you need to download the following tools:

* Android Studio or and equivalent software that emulates and Android device
* An IDE of your choice (Intellij, Eclipse, Netbeans)
* Appium Desktop

## Running the project

* Make sure to select and run an Android emulator from either Android Studio or from an external software:  
You will need to get the device name for later configurations
* Run Appium Desktop app and start a server.
* Once the server is up and running, start an Inspector session by clicking on the magnet icon on the top right of the server window.
* In the desired capabilities tab enter the following:  
```
{
  "automationName": "UiAutomator2",
  "platformName": "Android",
  "platformVersion": "9",
  "deviceName": "Nexus_5X_API_28",
  "app": "PATH_FOR_THE_APK"
}
```
* Start a Session
* Make sure you clone the repo and import it in the selected IDE.
* Add the required Jar files:
```
- Java client
- Selenium
- xmlbeans
- stax-api
- poi-ooxml
- gson
- curvesapi
- commons
- client-combined
```
* After adding all the jar files navigate to the `SelendroidTest.java` file in `src/com.mobile` directory and right click `Run As` Testng Test.

The emulator should now be running the tests and the results will be returned in the console stating that all test have passed successfully.

## Test Cases

1- Display Text View  

This test simply clicks on the `DisplayTextView` button and validate the test by checking the text displayed on the side if it matches.

2- Display Toast  

This test clicks on the `Display a toast` button to display a toast message on the screen.

3- Register a user  

This test is triggered by clicking on the `Show Progress Bar for a while` button, after the progress bar loads it then reads from an excel sheet and enters data respectively and register the user.

4- Text Field  

This is a simple test to enter a predefined text into the text field.

5- Touch Test  

This test is triggered by clicking on the `Touch test` button and then clicks on tap on the layout to confirm a single tap was made.

## License

Check license details [here](./LICENSE)

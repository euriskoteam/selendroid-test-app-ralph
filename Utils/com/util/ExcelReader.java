package com.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.user.User;

public class ExcelReader {

  public static List ReadExcelFile() throws IOException {
    List ExcelDataRows = new ArrayList();
    File file = new File("users.xlsx");
    FileInputStream fis = new FileInputStream(file);

    XSSFWorkbook wb = new XSSFWorkbook(fis);
    XSSFSheet sheet = wb.getSheetAt(0);

    Iterator<Row> rowIterator = sheet.iterator();

    while (rowIterator.hasNext()) {

      User user = new User();
      Row row = rowIterator.next();
      Iterator<Cell> cellIterator = row.cellIterator();

      while (cellIterator.hasNext()) {
        Cell cell = cellIterator.next();

        if (cell.getColumnIndex() == 0) {
          user.setUsername(String.valueOf(cell.getStringCellValue()));
        } else if (cell.getColumnIndex() == 1) {
          user.setEmail(String.valueOf(cell.getStringCellValue()));
        } else if (cell.getColumnIndex() == 2) {
          user.setPassword(String.valueOf(cell.getStringCellValue()));
        } else if (cell.getColumnIndex() == 3) {
          user.setName(String.valueOf(cell.getStringCellValue()));
        } else if (cell.getColumnIndex() == 4) {
          user.setLanguage(String.valueOf(cell.getStringCellValue()));
        } else if (cell.getColumnIndex() == 5) {
          user.setAds(Boolean.valueOf(cell.getBooleanCellValue()));
        }

      }

      ExcelDataRows.add(user);

    }

    wb.close();
    fis.close();
    return ExcelDataRows;

  }

  public static void main(String args[]) throws IOException {

    List userList = ReadExcelFile();
    System.out.println(userList);

  }

}

package com.user;

public class User {

    private String username;
    private String email;
    private String password;
    private String name;
    private String language;
    private Boolean ads;

    public User() {
    }

    public User(String username, String email, String password, String name, String language, Boolean ads) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.name = name;
        this.language = language;
        this.ads = ads;
    }

    @Override
    public String toString() {
        return username + "," + email + "," + password + "," + name + "," + language + "," + ads;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setAds(Boolean ads) {
        this.ads = ads;
    }

    public Boolean getAds() {
        return ads;
    }
}
